/* UI loader
 *
 * This part of JavaScript loads prelisted ui scripts automatically.
 */

require([], function($){

    var uiList = [
        'main',
        'contactlist',
        'chatbox',
        'login',
        'keyboard-signal',
    ];

    var requireList = [];
    for(var i in uiList){
        requireList.push('ui/' + uiList[i] + '/index');
    };

    webix.ready(function(){
        require(requireList, function(){});
    });

});
