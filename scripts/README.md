Notices on Developing Details of this System
============================================

The system utilizes a library called `postal`. It provides events-alike
signaling mechanism to organize the behavious of different parts of our
program. Understanding this will make it much easier to extend or modify
this program.

## Services: Modules of the System 

A service is a grouped part of scripts, they do similar jobs(e.g. interaction
with human, or do communication jobs with server). A service can be written
in one file, e.g. `xmpp.js`, or one starting file and one folder(`ui.js` and
`ui/`).

A service occupies mostly several channels(see next chapter).

### XMPP Service

The **XMPP service** manages one or more XMPP account and do communication with
the server. It's the driver part of our system.

XMPP service should implement several features of XMPP protocols:

1. Query and publish public keys.
2. Server-end storage.
3. Buddy list request, buddy addition.

### Identity Service

The **Identity Service**(not implemented) stores information(or even
intelligences) about the local user and his/her buddies, and provides an
abstracted interface of communication for **UI Service**. Its tasks includes:

1. Store and resume stored information within **Storage Service**.
2. Manages locally used XMPP accounts, including saving their passwords, and
   invoking **XMPP Service** to use such.
3. Manages buddies, group XMPP-level buddies as abstracted buddies.
4. Manages local private key.
5. Manages public keys of buddies.
6. Provides instructive and encryptive interfaces:
  1. For outgoing message, encryption service is provided.
  2. For incoming message, decryption service is provided.
  3. Publish the local public key on request.

The identity service relies on Storage Service. The latter may provide large
updates at anytime, and the Identity Service must be ready for that. Ideally
the Identity Service may not implement its own memory variable, but relies
on frequently lookups.

### Storage Service

(Not implemented)

The **Storage Service** is a comprehensive encrypted storage. It utilizes
as its backend multiple storage engines, e.g. `localStorage` and storage
function provided by `XMPP Service`.

The Storage Service is accountable. The `decrypt` login window at start of
program is its direct invokation, which deals nothing more than decrypting
the locally stored bootstrap information, which is then used to read the whole
storage of a given account.

The bootstrap information includes a salt, and an encrypted main key. This
piece of information is usually stored in localStorage, and will never be sent
to a network service, but can be exported and imported by the user. It is
therefore the most important info that a user should always hold and taken care
of.

After bootstrap information is decrypted, additional information can be
decrypted and loaded with this piece of knowledge to complete the resumation.
If there is more data in localStorage, then it will be used. If not, but anyway
a manually configured XMPP account is available, which makes connecting to
network possible, more resuming jobs can be done by asking the server for
previously stored memories.

The resumation of information will be done in a versioned way, that means,
the Storage Service sends each piece of resumable info in an encrypted,
authenticated and timestamp'ed form, so that no conflict will occur.



## Postal Signals: Detailed Explanation

### channel **network**

This channel is used for exchange low-level _communications_: to call XMPP
backend send messages, or XMPP backend use this to report a received message.

### channel **emergency**

#### emergency/declare

When emitted, it means the user have performed a declaration on emergency
situation. All session data should be wiped, and crypto keys in memory should
be deleted.
