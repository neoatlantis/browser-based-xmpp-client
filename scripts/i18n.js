define([], function(){
var translations = {
//////////////////////////////////////////////////////////////////////////////
'platform-name': 'ENERGY通信平台',

'contact-list:click-to-add-contact': '点击这里添加新联系人',

'login-window:window-title': '需要密码',
'login-window:description': '输入已有的用户名/密码登录，或输入新的凭据创建身份。',
'login-window:username-label': '用户名',
'login-window:username-hint': '选取一个，或输入新的',
'login-window:password1-label': '密码',
'login-window:password2-label': '确认密码',
'login-window:submit-button': '解锁',

'emergency-countdown-window:window-title': '紧急关闭系统倒计时',
'emergency-countdown-window:description': '按ESC取消，按任意键立即执行',


//////////////////////////////////////////////////////////////////////////////
};
return function(s){
    if(translations[s]) return translations[s];
    return '!' + s;
};

});
