require([
    'i18n', 
    'ui/main/design'
], function(i18n){
//////////////////////////////////////////////////////////////////////////////

$$("mainleft").addView({
    id: "add-contact",
    view:"button",
    label: i18n('contact-list:click-to-add-contact'),
});

var design = {
    id: "contact-list",
    view: "list",
    autoheight: true,
    data: [
        {name: 'a'},
        {name: 'b'},
    ],
    select: true,
    template: "<strong>#name#</strong>",
};

$$("mainleft").addView(design);

//////////////////////////////////////////////////////////////////////////////
});
