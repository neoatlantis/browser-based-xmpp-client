define(['i18n'], function(i18n){
//////////////////////////////////////////////////////////////////////////////

webix.ui.fullScreen();

var design = {
    container: "basic",
    rows: [
        {type: "header", template: i18n("platform-name"), autoheight: true},
        {
            id: 'maincolumns',
            cols: [
                {view: "layout", id:"mainleft", rows: [], gravity: 0.5},
                {view: "resizer"},
                {view: "layout", id: "mainmiddle", rows: []},
                {view: "resizer"},
                {view: "layout", id: "mainright", rows: [], gravity: 0.6},
            ],
        }
    ]
};

var uiMain = webix.ui(design);
webix.event(window, 'resize', function(){ uiMain.adjust(); });

//////////////////////////////////////////////////////////////////////////////
});
