require(['i18n'], function(i18n){
//////////////////////////////////////////////////////////////////////////////

var body = {
    view: 'form',
    rows: [
        {
            view: 'label',
            label: i18n('login-window:description'),
            autowidth: true,
            autoheight: true,
        },
        {
            view: 'combo',
            id: 'login-username',
            label: i18n('login-window:username-label'),
            placeholder: i18n('login-window:username-hint'),
        },
        {
            view: 'text',
            id: 'login-password1',
            type: 'password',
            label: i18n('login-window:password1-label'),
        },
        {
            view: 'text',
            id: 'login-password2',
            type: 'password',
            label: i18n('login-window:password2-label'),
        },
        {view: 'button', label: i18n('login-window:submit-button')},
    ],
};

var form = {
    view: 'window',
    id: 'login-window',
    head: i18n('login-window:window-title'),
    body: body,
    position: 'center',
    modal: true,
};

webix.ui(form);

//////////////////////////////////////////////////////////////////////////////
});
