require([
    'postal',
    'ui/login/design',
], function(postal){
//////////////////////////////////////////////////////////////////////////////

var $ui$ = postal.channel('ui');

function showLogin(data, env){
    var usernames = data.usernames || [];
    $$('login-username').define('options', usernames);

    $$('login-window').show();
};

$ui$.subscribe('show.login', showLogin);

/*showLogin({
    usernames: ['aaaaa', 'bbbbb'],
});  //debug*/

//////////////////////////////////////////////////////////////////////////////
});
