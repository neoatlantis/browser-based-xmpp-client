require([
    'jquery', 'postal',
    'ui/keyboard-signal/design',
], function($, postal){
//////////////////////////////////////////////////////////////////////////////
/*
 * This part of UI scripts defines some very special keyboard signals, using
 * which the system may e.g. perform an emergency shutdown.
 */



var emergencyCountdownNumber = -1;

function declareEmergency(){
    postal.channel('emergency').publish('declare', {});
    console.log("EMERGENCY declared!");
    endEmergencyCountdown();
};

function endEmergencyCountdown(){
    $$('emergency-countdown-window').hide();
    emergencyCountdownNumber = -1;
};

function emergencyCountdown(){
    if(emergencyCountdownNumber <= 0) return;
    emergencyCountdownNumber -= 1;
    $$('emergency-countdown-seconds').define(
        'label',
        emergencyCountdownNumber.toString()
    );
    $$('emergency-countdown-seconds').refresh();
    if(emergencyCountdownNumber <= 0){
        declareEmergency();
        return;
    };
    setTimeout(emergencyCountdown, 1000);
};

function startEmergencyCountdown(){
    if(emergencyCountdownNumber >= 0) return;
    $$('emergency-countdown-window').show();
    emergencyCountdownNumber = 20;
    emergencyCountdown();
};


$(window).keydown(function(e){
    if(emergencyCountdownNumber >= 0){
        // if emergency countdown is in progress
        if(27 == e.keyCode){
            endEmergencyCountdown();
        } else {
            declareEmergency();
        };
        e.preventDefault();
        return false;
    };
    if(e.ctrlKey && e.shiftKey && e.altKey){
        // when Ctrl+Shift+Alt are pressed together, start emergency countdown
        startEmergencyCountdown();
        e.preventDefault();
        return false;
    };
});


//////////////////////////////////////////////////////////////////////////////
});
