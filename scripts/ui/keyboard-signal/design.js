require(['i18n'], function(i18n){
//////////////////////////////////////////////////////////////////////////////

var design = {
    view: 'window',
    id: 'emergency-countdown-window',
    head: i18n('emergency-countdown-window:window-title'),
    modal: true,
    position: 'center',
    body: {
        rows: [
            {
                view: 'label',
                label: i18n('emergency-countdown-window:description'),
                align: 'center',
            },
            {
                view: 'label',
                id: 'emergency-countdown-seconds',
                label: '5',
                align: 'center',
            },
        ]
    },
};

webix.ui(design);

//////////////////////////////////////////////////////////////////////////////
});
