require(['jquery', 'ui/main/design'], function($){
//////////////////////////////////////////////////////////////////////////////

var longtext = '';
for(var i=0; i<1000; i++) longtext += i.toString() + '---<br />';

$('<div>', {id: 'history'}).appendTo('body').html(longtext).hide();

var historybox = {
    id: "chat-history",
    view: "scrollview",
    scroll: "y",
    height: 700,
    body: {
        rows: [
            {template: 'html->history', id: 'history', autoheight: true},
        ],
    },
};

$$("mainmiddle").addView(historybox);

var inputbox = {
    id: "chat-input",
    view: "form",
    rows: [
        {view: "textarea", minHeight: 100},
    ],
};

$$("mainmiddle").addView(inputbox);

//////////////////////////////////////////////////////////////////////////////
});
