require(['postal'], function(postal){
//////////////////////////////////////////////////////////////////////////////

/* 
XMPP backend: Automatic Manager of Network Connection and Communications
========================================================================

# Listening Behaviour

+------------+---------------------+-----------------------------------------+
| Channel    | Topic               | Description                             |
+------------+---------------------+-----------------------------------------+
| crypto     | publickey.broadcast | Record the broadcasted public key, to   |
|            |                     | answer network queries.                 |
| account    | activate            | Use a given account in network          |
|            |                     | communication.                          |
|            | deactivate          | Discard using given account for         |
|            |                     | communication.                          |
| session    | status              | Set all associated accounts into this   |
|            |                     | status.                                 |
| network    | send                | Send payload.                           |
|            | buddy.approval      | Approve given buddy addition request.   |
+------------+---------------------+-----------------------------------------+

# Publishing Behaviour 

+------------+---------------------+-----------------------------------------+
| Channel    | Topic               | Description                             |
+------------+---------------------+-----------------------------------------+
| account    | failure.auth        | Inform that this account have ever      |
|            |                     | experienced authentication failure.     |
| network    | receive             | Publishes a new message received from   |
|            |                     | network side.                           |
|            | buddy.request       | Publishes a buddy addition request.     |
+------------+---------------------+-----------------------------------------+

# Notes

* This backend assumes the other modules will do following jobs:
    1. `crypto/publickey.broadcast` will be sent at first decryption of private
       storage.
    2. After accounts were decrypted, accounts that should be active will be
       broadcasted using `account/activate`.

*/
var $internal$ = postal.channel('__internal__xmpp');
var $account$ = postal.channel('account'),
    $network$ = postal.channel('network'),
    $session$ = postal.channel('session');


function XMPPInstance(){
    var self = this;

    var url = 'http:';
    if('https:' == window.location.protocol) url = 'https:';
    url += '//neoatlantis.info/http-bind';
    var conn = new Strophe.Connection(url);


    function onConnectionStatusChanged(stat, err){
        var s = Strophe.Status;
        console.log('XMPP', stat);
        if(stat == s.CONNECTING) return onConnecting();
        if(stat == s.CONNFAIL) return onConnfail();
        if(stat == s.AUTHENTICATING) return onAuthenticating();
        if(stat == s.AUTHFAIL) return onAuthfail();
        if(stat == s.CONNECTED) return onConnected();
        if(stat == s.DISCONNECTED) return onDisconnected();
        if(stat == s.DISCONNECTING) return onDisconnecting();
        if(stat == s.ATTACHED) return onAttached();
        onError();
    };

    function onAuthfail(){
        doAutoReconnection = false;
    };

    function onDisconnected(){
        if(doAutoReconnection) doConnect();
    };

    function onConnecting(){};
    function onConnfail(){};
    function onAuthenticating(){};
    function onConnected(){};
    function onDisconnecting(){};
    function onAttached(){};

    /* Connectivity Auto Management */
    
    var doAutoReconnection = false, savedJID, savedPwd;

    function doConnect(jid, pwd){
        if(!jid && !pwd){
            jid = savedJID;
            pwd = savedPwd;
        } else {
            savedJID = jid;
            savedPwd = pwd;
        };
        conn.connect(jid, pwd, onConnectionStatusChanged);
    };
    this.connect = function(jid, pwd){
        doAutoReconnection = true;
        doConnect(jid, pwd);
    };

    this.disconnect = function(){
        doAutoReconnection = false;
        conn.disconnect();
    };

    /* Message sending and receiving */

    this.send = function(jid, message){
        // TODO handle condition when we can't send using this instance
        var msg = $msg({to: jid, type: 'chat', xmlns: 'jabber:client'});
        msg.c('body').t(message);
        conn.send(msg);
    };

    conn.addHandler(function recvMessage(s){
        $internal$.publish('receive', {
            data: s,
            toJID: jid,
        });
        return true;
    }, null, 'message', 'chat');

    /* Presence setting */

    this.setPresence = function(value){
    };

    
    return this;
};

/****************************************************************************/

var xmppInstances = {};

/* `internal/receive`, when one instance reports it has got a new message
 * from network. */

$internal$.subscribe('receive', function(data, env){
    // TODO filter the data basically
    console.log(data);
    $network$.publish('receive', data);
});

/* `account/activate`, receives account configuration for a new XMPP
 * connection, this account will then be managed by this module. */

$account$.subscribe('activate', function(data, env){
    var jid = data.jid, pwd = data.password;
    if(!xmppInstances[jid]) xmppInstances[jid] = new XMPPInstance();
    xmppInstances[jid].connect(jid, pwd);
    console.log("Activate XMPP account: " + jid);
});

/* `network/send`, receives a sending command. */

$network$.subscribe('send', function(data, env){
    var jid = data.jid, msg = data.msg;
    // TODO logic for selecting a XMPP client
});

//////////////////////////////////////////////////////////////////////////////
});
