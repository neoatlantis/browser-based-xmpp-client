require([
    'postal',
    'ui/login/index',
], function(postal){
//////////////////////////////////////////////////////////////////////////////

var $emergency$ = postal.channel('emergency'),
    $storage$ = postal.channel('storage'),
    $ui$ = postal.channel('ui');

var decrypted = false;
var tempStorage = {};


function persistStorage(){
    if(!decrypted) return onNotDecrypted();
    // TODO encryption
    localStorage.setItem('storage', JSON.stringify(tempStorage));
};

function onNotDecrypted(){
    $ui$.publish('show.login', {});
};

/****************************************************************************/

$emergency$.subscribe('declare', function(data, env){
    console.log("Storage purged due to EMERGENCY state.");
    decrypted = false;
    tempStorage = {};
});


$storage$.subscribe('decrypt', function(data, env){
    if(decrypted) return;
    var username = data.username, password = data.password;
    // Decrypt or create a user account TODO
});

$storage$.subscribe('import.bootstrap', function(data, env){
    // TODO
});

$storage$.subscribe('export.bootstrap', function(data, env){
    // TODO call the show.bootstrap on UI channel to display bootstraps
});


/* Storage are run in a query-response schema. Use `get.***` to send a query,
 * but it does not guarantee the corresponding response will always be
 * delivered. And if there is a response, all who have subscribed to `got.***`
 * will be informed. */

$storage$.subscribe('get.#', function(data, env){
    var key = env.topic.substring(4);
    if(decrypted){
        if(tempStorage[key] != undefined){
            var answer = tempStorage[key]['value'];
            $storage$.publish('got.' + key, answer);
            return;
        };
    } else {
        onNotDecrypted();
    };
});

$storage$.subscribe('set.#', function(data, env){
    if(!decrypted) return onNotDecrypted();
    var key = env.topic.substring(4),
        value = data;
    tempStorage[key] = {
        time: Date.now(),
        value: value,
    };
    $storage$.publish('got.' + key, value);
    persistStorage();
});


decrypted = true; // XXX debug only
try{
    tempStorage = JSON.parse(localStorage.getItem('storage')) || {};
} catch(e){
    tempStorage = {};
}

$storage$.subscribe('got.test', function(d){ console.log(d); });
$storage$.publish('get.test');
//$storage$.publish('set.test', 'obj');

//////////////////////////////////////////////////////////////////////////////
});
